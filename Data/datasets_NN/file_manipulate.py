# ****** 5/10/2018 ******
# *** iterate specific folder and move all the image files ***

import os
import cv2


def file_walk_move_image (root_path, target_root):

    list_dir = os.walk(root_path)
    for root, dirs, files in list_dir:
        for f in files:
            if f[-4:] == '.jpg' or f[-4:] == '.png':
                img_path = os.path.join(root, f)
                print(img_path)
                img = cv2.imread(img_path)
                cv2.imwrite(os.path.join(target_root, f), img)


file_walk_move_image('7676-8047', 'image')