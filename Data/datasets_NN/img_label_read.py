# ****** 5/10/2018 ******
# *** read labels and images ***
# *** flexible postfix .jpg or .png ***

import cv2
import numpy as np


def expand(label, c=10):
    # return y : (num_class, num_samples)
    y = np.eye(c)[label]
    y = np.squeeze(y)
    return y


def img_read(img_name):

    try:
        img = cv2.imread(img_name + ".jpg")
        img = cv2.resize(img, (64, 64))
    except cv2.error:
        img = cv2.imread(img_name + ".png")
        img = cv2.resize(img, (64, 64))

    print("read image successfully")
    return img


def iterate_path(file, root_path='dataset_house/image/'):

    imgs = []
    label = []
    for line_num, line in enumerate(file):

        if line[:len(line) - 1] == '':
            print('****************enter*******************')
            break

        if line_num % 2 == 0:
            line = root_path + line[:-1]
            print(line)
            img = img_read(line)
            imgs.append(img)
        else:
            label.append(int(line[:-1]))
            print(line[:-1])

    label.append(int(line))

    print(np.shape(imgs))
    print(len(imgs))
    # label.append(1)

    return imgs, label


filename = 'dataset_house/train.txt'
file = open(filename, 'r')

imgs, label = iterate_path(file)

print(label)
label = expand(label, 2)
print(np.shape(label))


print(np.shape(imgs[2]))
print(label[-1])
cv2.imshow("img", imgs[2])
cv2.waitKey(0)
