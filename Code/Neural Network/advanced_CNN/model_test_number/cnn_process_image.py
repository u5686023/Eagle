# ****** read and flatten the images ******
# *** expand the label into one-hot ***
# *** imgs : (number_sample, img_fei, img_wid, channel) ***
# *** label: (number_sample, 1) ***

# ****** 10/9/2018 ******
# *** add the output: imgs ***

# ****** 12/09/2018 ******
# *** adapt the cnn version ***
# *** remove the output: data ***

import numpy as np
import cv2


# transfer int into c dimensions one-hot array
def expand(label, c=10):
    # return y : (num_class, num_samples)
    y = np.eye(c)[label]
    y = np.squeeze(y)
    return y


def load_image(path='train.txt', img_wid=64, img_high=64):
    # load the image and label
    # return the flatten data of images and int labels
    # imgs : (number_sample, img_fei, img_wid, channel)
    # label : (num_samples, 1)

    filename = path
    file = open(filename, 'r')
    path = []

    label = []   # label for each img
    imgs = []    # original img
    for i, line in enumerate(file):
        # ignore the last line
        if line[:len(line) - 1] == '':
            imgs = imgs[:-1]
            break

        if i % 2 == 0:
            lines = line.replace('\\', '/')[:len(line) - 1]
            path.append(lines)
            # reshape and normalize the image to single dimension data
            img = cv2.imread(lines)
            img = cv2.resize(img, (img_wid, img_high))
            imgs.append(img)
        elif line[:len(line) - 1] != '':
            lines = int(line[:len(line) - 1])
            label.append(lines)

    # disorder the data and label
    np.random.seed(0)
    label = np.random.permutation(label)
    np.random.seed(0)
    imgs = np.random.permutation(imgs)

    # reshape into (dimension, num_class)
    label = np.reshape(label, (label.shape[0], 1))

    print('org data shape *  :' + str(np.shape(imgs)))
    print('org label shape * :' + str(np.shape(label)))
    return imgs, label




