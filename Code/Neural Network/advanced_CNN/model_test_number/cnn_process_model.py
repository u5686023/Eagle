# Mulong Xie
# @ Australian National University

# ****** 12/09/2018 ******

# ****** 13/09/2018 ******
# *** add the predict functionality ***

import cnn_process_CNN as nn
import cnn_process_image as ip
import cv2
import numpy as np

# *** load the flatten image data and decimal number label ***
img, label = ip.load_image('train.txt')

# *** transfer the label into class_number dimensions matrix ***
y = ip.expand(label, 10)  # label has 10 classes

# *** split the train and test data ***
x_train_org = img[:-40]
x_train = x_train_org / 255.
y_train = y[:-40]
x_test_org = img[-40:]
x_test = x_test_org / 255.
y_test = y[-40:]

print(np.shape(x_train))
print(np.shape(y_train))

# *** train the NN model ***
train = False
if train:
    nn.train(x_train, y_train, x_test, y_test, 500)

# *** test and predict ***
test_index = 22
y_hat = nn.predict(x_test_org[test_index])
cv2.imshow('img', x_test_org[test_index])
cv2.waitKey(0)
