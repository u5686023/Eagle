# ****** 12/09/2018 ******
# *** usable 3*conv + 2*fc model ***
# *** x:(number_sample, img_fei, img_wid, channel) ***
# *** y:(number_sample, number_class) ***
# *** bug: the y_hat and y_label should not be put into tf.transpose() ***
# *** bug: the calculation of accuracy should use the tf.argmax(y,1) ***

# ****** 13/09/2018 ******
# *** add the drop_out functionality ***
# *** separate the functions ***

# ****** 13/09/2018 ******
# *** add the predict functionality ***

import tensorflow as tf
import numpy as np


def weight(shape):
    initia = tf.random_normal(shape)  # (mask_weight, mask_hei, n_l-1, number_mask)
    return tf.Variable(initia, dtype=tf.float32)


def bias(shape):
    initia = tf.constant(0.01, shape=shape)
    return tf.Variable(initia, dtype=tf.float32)


def init_parameters():

    w1 = weight((5, 5, 3, 8))
    b1 = bias([8])
    w2 = weight((3, 3, 8, 16))
    b2 = bias([16])
    w3 = weight((2, 2, 16, 32))
    b3 = bias([32])

    parameters = {'w1': w1, 'b1': b1,
                  'w2': w2, 'b2': b2,
                  'w3': w3, 'b3': b3}
    return parameters


def conv(x, w, name, padding='SAME'):
    return tf.nn.conv2d(x, w, strides=[1, 1, 1, 1], padding=padding, name=name)


def max_pooling(x, name, padding='VALID'):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding=padding, name=name)


def forward(x, parameters, keep_drop):
    w1 = parameters['w1']
    b1 = parameters['b1']
    w2 = parameters['w2']
    b2 = parameters['b2']
    w3 = parameters['w3']
    b3 = parameters['b3']

    z1 = conv(x, w1, 'z1') + b1
    a1 = tf.nn.relu(z1, name='a1')
    p1 = max_pooling(a1, 'p1')

    z2 = conv(p1, w2, 'z2') + b2
    a2 = tf.nn.relu(z2, name='a2')
    p2 = max_pooling(a2, 'p2')

    z3 = conv(p2, w3, 'z3') + b3
    a3 = tf.nn.relu(z3, name='a3')
    p3 = max_pooling(a3, 'p3')

    f1 = tf.contrib.layers.flatten(p3)
    fc1 = tf.contrib.layers.fully_connected(f1, 1024)
    fc1_drop = tf.nn.dropout(fc1, keep_drop)
    y_hat = tf.add(tf.contrib.layers.fully_connected(fc1_drop, 10, activation_fn=None), 0, name='y_hat')

    return y_hat


# x: (number_sample, img_hei, img_wid, img_channel)
# y: (number_sample, number_classes)
def train(x_train, y_train, x_test, y_test, num_iter, learning_rate=0.001, save_path='./trained_model/cnn'):
    tf.reset_default_graph()

    x_shape = x_train.shape
    d_y = y_train.shape[1]  # get the dimension of the label (number of classes)

    # create the tensor
    x = tf.placeholder(shape=(None, x_shape[1], x_shape[2], x_shape[3]), dtype=tf.float32, name='x')  # None is the flexible sample number
    y = tf.placeholder(shape=(None, d_y), dtype=tf.float32, name='y')
    keep_drop = tf.placeholder(tf.float32, name='keep_drop')

    # create the variables
    parameters = init_parameters()
    # and forward to calculate the y_hat
    y_hat = forward(x, parameters, keep_drop)
    # calculate the cost
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=y_hat, labels=y))
    # train the model
    train = tf.train.AdamOptimizer(learning_rate).minimize(cost)

    # initialize
    init = tf.global_variables_initializer()

    # run the session
    with tf.Session() as sess:
        sess.run(init)

        for i in range(num_iter):
            _, costs = sess.run([train, cost], feed_dict={x: x_train, y: y_train, keep_drop: 0.8})

            if i % 20 == 0:
                print('the ' + str(i) + 'th iteration')
                print(costs)

                # calculate the accuracy
                # boolean array, element-wise compare, choose the max probability of class for each image
                correct = tf.equal(tf.argmax(y_hat, 1), tf.argmax(y, 1))
                # convert to float and calculate the mean correct rate
                accuracy = tf.reduce_mean(tf.cast(correct, 'float'))

                print('train accuracy:', accuracy.eval({x: x_train, y: y_train, keep_drop: 1}))
                print('test accuracy:', accuracy.eval({x: x_test, y: y_test, keep_drop: 1}))

        saver = tf.train.Saver()
        saver.save(sess, save_path)
    return True


def predict(x, load_path='./trained_model/cnn.meta'):

    x = np.reshape(x, (1, x.shape[0], x.shape[1], x.shape[2]))  # transform the x (64,64,3) into (1,64,64,3)
    drop_prob = 1   # the another input placeholder in this pretrained model

    with tf.Session() as sess:
        saver = tf.train.import_meta_graph(load_path)
        saver.restore(sess, tf.train.latest_checkpoint('./trained_model'))

        graph = tf.get_default_graph()
        x_tf = graph.get_tensor_by_name('x:0')
        drop_prob_tf = graph.get_tensor_by_name('keep_drop:0')
        y_hat = graph.get_tensor_by_name('y_hat:0')
        y_pre = tf.argmax(y_hat, 1)

        print(sess.run(y_pre, feed_dict={x_tf: x, drop_prob_tf: drop_prob}))

    return y_pre


print('*** CNN Eagle ***')
