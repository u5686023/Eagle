# Mulong Xie
# @ Australian National University

# ****** 12/09/2018 ******

# ****** 13/09/2018 ******
# *** add the predict functionality ***

# ****** 05/10/2018 ******
# *** testing house dataset ***

# ****** 06/10/2018 ******
# *** add load image switch ***
# *** add self-import predict test func ***

import cnn_process_CNN as nn
import cnn_process_image_house as iph
import cv2
import numpy as np

load_image = True
if load_image:
    # *** load the image data and decimal number label ***
    img, label = iph.load_image_join_path()

    # *** transfer the label into class_number dimensions matrix ***
    y = iph.expand(label, 2)  # label has 2 classes

    # *** split the train and test data ***
    x_train_org = img[:-200]
    x_train = x_train_org / 255.
    y_train = y[:-200]
    x_test_org = img[-200:]
    x_test = x_test_org / 255.
    y_test = y[-200:]

    print(np.shape(x_train))
    print(np.shape(y_train))

# *** train the NN model ***
train = False
if train:
    nn.train(x_train, y_train, x_test, y_test, 660)

# *** test and predict ***
import_img = False
if import_img:
    test_img = cv2.imread('1.jpg')
    test_img = cv2.resize(test_img, (64, 64))
else:
    test_index = 162
    test_img = x_test_org[test_index]

y_hat = nn.predict(test_img)
cv2.imshow('img', cv2.resize(test_img, (200, 200)))
cv2.waitKey(0)
