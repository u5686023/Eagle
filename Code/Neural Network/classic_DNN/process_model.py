# Mulong Xie
# @ Australian National University

# ****** 10/9/2018 ******
# *** add the predict function ***
# *** add the output of the prediction ***

import tensorflow as tf
import cv2
import numpy as np
import process_image as ip
import process_NN as nn


# *** load the flatten image data and decimal number label ***
data, label, imgs = ip.load_image('D:/train/train.txt')

# *** transfer the label into class_number dimensions matrix ***
y = ip.expand(label, 10)

# *** split the train and test data ***
x_train = data[:, :-40]
y_train = y[:, :-40]
x_test = data[:, -40:]
y_test = y[:, -40:]

# *** train the NN model ***
train = False  # retrain the model or not
if train:
    parameter = nn.train(x_train, y_train, x_test, y_test, 600, save_path='./trained_model/dnn')

# *** test and predict ***
test_index = 26
y_hat = nn.predict(x_train[:, test_index], load_path='./trained_model/dnn.meta')
cv2.imshow('img', imgs[test_index])
cv2.waitKey(0)