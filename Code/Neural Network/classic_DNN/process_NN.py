# Mulong Xie
# @ Australian National University

# ****** 10/9/2018 ******
# *** change the get_variable into Variable ***
# *** fix the incorrect accuracy bug after the first run ***
# *** problem: the initializer of the w and b were incorrect ***

# ****** 10/9/2018 ******
# *** add the predict function ***
# *** add the save trained model function ***

# ****** 11/0/2018 ******
# *** optimize the predict function ***
# *** instead of fetching and reconstruct the graph ***
# *** using the get_tensor_byname() to fetch the operation directly ***

import tensorflow as tf
import numpy as np


# create the single w
def weight(shape, name):
    initial = tf.random_normal(shape, stddev=0.1)
    return tf.Variable(initial, name=name)


# create the single b
def bias(shape, name):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial, name=name)


# 3 layers classical NN model
def create_variables():

    w1 = weight([25, 12288], 'w1')  # 64*64*3 # [n1,n0]
    b1 = bias([25, 1], 'b1')
    w2 = weight([12, 25], 'w2')     # [n2, n1]
    b2 = bias([12, 1], 'b2')
    w3 = weight([10, 12], 'w3')
    b3 = bias([10, 1], 'b3')

    parameters = {'w1': w1, 'b1': b1,
                  'w2': w2, 'b2': b2,
                  'w3': w3, 'b3': b3}

    return parameters


# the linear function used in forwarding
def linear(x, w, b, name):
    y = tf.add(tf.matmul(w, x), b, name=name)
    return y


# the forward function built on the parameters
def forward(parameters, x):
    w1 = parameters['w1']
    b1 = parameters['b1']
    w2 = parameters['w2']
    b2 = parameters['b2']
    w3 = parameters['w3']
    b3 = parameters['b3']

    z1 = linear(x, w1, b1, 'z1')
    a1 = tf.nn.relu(z1, name='a1')
    z2 = linear(a1, w2, b2, 'z2')
    a2 = tf.nn.relu(z2, name='a2')
    z3 = linear(a2, w3, b3, 'z3')

    return z3


def train(x_train, y_train, x_test, y_test, num_iter, learning_rate = 0.001, save_path='./trained_model/dnn'):

    (n_x, m) = x_train.shape
    n_y = y_train.shape[0]

    # create the tensor
    x = tf.placeholder(shape=(n_x, None), dtype=tf.float32, name='x')  # None is the flexible dimension
    y = tf.placeholder(shape=(n_y, None), dtype=tf.float32, name='y')

    # create the variables
    parameters = create_variables()  # as the form of tf.variable and already be restored

    # define the op
    # calculate the y^
    y_hat = forward(parameters, x)
    # calculate the cost

    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=tf.transpose(y_hat), labels=tf.transpose(y)))
    # define a optimizer
    optimizer = tf.train.AdamOptimizer(learning_rate)
    # define the train op
    train = optimizer.minimize(cost)

    # initialize
    init = tf.global_variables_initializer()

    # run the session
    with tf.Session() as sess:
        sess.run(init)

        for i in range(num_iter):
            _, costs = sess.run([train, cost], feed_dict={x: x_train, y: y_train})

            if i % 100 == 0 :
                print('the ' + str(i) + 'th iteration')
                print(costs)

                # calculate the accuracy
                # boolean array, element-wise compare, choose the max probability of class for each image
                correct = tf.equal(tf.argmax(y_hat,), tf.argmax(y))
                # convert to float and calculate the mean correct rate
                accuracy = tf.reduce_mean(tf.cast(correct, 'float'))

                print('train accuracy:', accuracy.eval({x: x_train, y: y_train}))
                print('test accuracy:', accuracy.eval({x:x_test, y: y_test}))

        parameters = sess.run(parameters)  # the parameters is the current value in the graph

        # save the model
        saver = tf.train.Saver()
        saver.save(sess, save_path)

        sess.close()

    return parameters


# predict
def predict(x, load_path='./trained_model/dnn.meta'):

    x = np.reshape(x, (x.shape[0], 1))

    with tf.Session() as sess:
        saver = tf.train.import_meta_graph(load_path)
        # check the file '.trained_model/checkpoint'
        saver.restore(sess, tf.train.latest_checkpoint('./trained_model'))

        # fetching the last operation z3 and the placeholder x in the imported model
        # no need to fetch the parameters explicitly
        # just using the last layer operation directly
        graph = tf.get_default_graph()
        x_tf = graph.get_tensor_by_name('x:0')
        z3 = graph.get_tensor_by_name('z3:0')
        y_pre = tf.argmax(z3)

        print(sess.run(y_pre, feed_dict={x_tf: x}))
        sess.close()

    return y_pre

