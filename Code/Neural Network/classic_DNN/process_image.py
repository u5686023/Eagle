# Mulong Xie
# @ Australian National University

# ****** read and flatten the images ******
# ***** expand the label into one-hot *****
# return the data (dimension, num_samples)
# *** return the label (10, um_samples) ***

# ****** 10/9/2018 ******
# *** add the output of the imgs ***

import numpy as np
import cv2

# transfer int into c dimensions one-hot array
def expand(label, c=10):
    # return y : (num_class, num_samples)
    y = np.eye(c)[label]
    y = np.squeeze(y)
    y = y.T
    return y


def load_image(path='train.txt', img_wid=64, img_high=64):
    # load the image and label
    # return the flatten data of images and int labels
    # data  : (num_samples, dimension)
    # label : (num_samples, 1)
    # imgs  : resized img

    filename = path
    file = open(filename, 'r')
    path = []

    label = []   # label for each img
    data = []    # flatten data
    imgs = []    # original img
    for i, line in enumerate(file):
        # ignore the last line
        if line[:len(line) - 1] == '':
            data = data[:-1]
            break

        if i % 2 == 0:
            lines = line.replace('\\', '/')[:len(line) - 1]
            path.append(lines)
            # reshape and normalize the image to single dimension data
            img = cv2.imread(lines)
            img = cv2.resize(img, (img_wid, img_high))
            imgs.append(img)
            data.append(img.reshape((img_wid*img_high*3)) / 255.)
        elif line[:len(line) - 1] != '':
            lines = int(line[:len(line) - 1])
            label.append(lines)

    # disorder the data and label
    np.random.seed(0)
    data = np.random.permutation(data)
    np.random.seed(0)
    label = np.random.permutation(label)
    np.random.seed(0)
    imgs = np.random.permutation(imgs)

    # reshape into (dimension, num_class)
    data = np.float32(data.T)
    label = np.reshape(label, (1, -1))

    print('org data shape   :' + str(np.shape(data)))
    print('org label shape  :' + str(np.shape(label)))
    return data, label, imgs


# data, label = load_image('train.txt')

# print(np.shape(label))
# print(label[:10])
# y = expand(label[:10], 10)
# print(np.shape(y))
# print(np.shape(data))
# print(y)

# cv2.imshow('img', np.reshape(data[4], (64, 64, 3)))
# cv2.waitKey(0)

