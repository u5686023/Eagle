import numpy as np
import tensorflow as tf

# **** get the training data ****
sample = np.array([[1], [-10], [25]])     # the real data of the training set will be assigned to the x

# **** declare the structure of the model ****
x = tf.placeholder(tf.float32, [3, 1])  # training data will assign later
w = tf.Variable(0, dtype=tf.float32)   # the parameters need to be learned by training
cost = x[0][0] * w ** 2 + x[1][0] * w + x[2][0]       # cost function with the training set ***need to be classified***
# cost = w**2 - 10*w + 25                             # cost function with constant coefficients
# cost = tf.add(tf.add(w**2, tf.multiply(-10.,w)), 2) # complex expression
train = tf.train.GradientDescentOptimizer(0.01).minimize(cost)  # the way to update the parameters
init = tf.global_variables_initializer()              # initialize the parameters(w,b)

# **** initialization test ****
ses = tf.Session()
ses.run(init)
print(ses.run(w))

# **** single training test ****
ses.run(train, feed_dict={x: sample})  # use the sample to instance the x and feed the model
print(ses.run(w))

# **** multiple training test ****
for i in range(1000):
    ses.run(train, feed_dict={x: sample})
print(ses.run(w))

ses.close()

