import os

root_dir = os.path.dirname(__file__)
raster_input_path = root_dir + "/act5.tif"
print(raster_input_path)

ID_path = root_dir + "/act5_id.txt"
def read_IDs(ID_path):
    with open(ID_path) as f:
        content = f.read().splitlines()
    content = [x.strip() for x in content]
    return content
    # print(content)
    # print(len(content))
    f.close()


IDs = read_IDs(ID_path)
print(IDs)

f1 = open('id2.txt','w+')
for i in range(0, len(IDs)):
    IDs[i] = int(IDs[i])

IDs.sort()

for id in range(700, 801):
    s = str((IDs[id]))+ "_1\n"
    f1.write(s)
    f1.write("\n")

    s = str((IDs[id])) + "_2\n"
    f1.write(s)
    f1.write("\n")

    s = str((IDs[id])) + "_3\n"
    f1.write(s)
    f1.write("\n")