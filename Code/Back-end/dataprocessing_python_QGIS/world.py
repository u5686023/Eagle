import numpy as np
import collections as cl
import itertools as itls
def UDG(A,n):
    G = [0] * n
    for i in range(n):
        G[i] = [0] * n
    for i in range(len(A)):
        u,v = A[i]
        G[u][v] = 1
        G[v][u] = 1
    return G


def bfs(map, x, y):
    find = cl.OrderedDict()
    touched = cl.OrderedDict()
    front = 0
    tail = 1
    count = 0

    find[str(x) + str(y)] = (x, y)
    while front < tail:

        print(list(find.keys())[front])
        (xt, yt) = find[list(find.keys())[front]]

        front += 1

        count += 1
        touched[str(xt) + str(yt)] = 1

        (it1, it2) = (0 if xt - 1 < 0 else xt - 1, np.shape(map)[0] - 1 if xt + 1 > np.shape(map)[0] - 1 else xt + 1)
        (jt1, jt2) = (0 if yt - 1 < 0 else yt - 1, np.shape(map)[1] - 1 if yt + 1 > np.shape(map)[1] - 1 else yt + 1)
        # print("it1: " + str(it1) + " it2: " + str(it2))
        # print("jt1: " + str(jt1) + " jt2: " + str(jt2))
        cross = list(itls.product((it1, it2), (jt1, jt2)))
        print(cross)

        for c in cross:
            (i, j) = c
            s = str(i) + str(j)
            if map[i][j] != 0 and (s not in touched.keys()) and (s not in find.keys()):
                print(str(s) + str(s not in find.keys()))
                find[s] = (i, j)
                tail += 1

        print("******************************")
    return count

def DFS(G):
    V = len(G)
    color = ['w'] * V
    for i in range(V):
        color[i] = ['w'] * V
    par = [None] * V
    disconnect = []
    for u in range(V):
        for v in range(V):
            if color[u][v] == 'w' and G[u][v] == 1:
                elements = (u)
                elements = DFS_Visit(G,u,v,color,par,elements)
    disconnect.append(elements)    
    return disconnect

def DFS_Visit(G,u,v,color,par,elements):
    color[u][v] = 'g'
    try:
        if G[u-1][v] == 1 and color[u-1][v] == 'w':
            elements.append(v)
            elements = DFS_Visit(G,u-1,v,color,par,elements)
    except IndexError:
        None
    try:
        if G[u][v-1] == 1 and color[u][v-1] == 'w':
            elements.append(v)
            elements = DFS_Visit(G,u,v-1,color,par,elements)
    except IndexError:
        None
    try:
        if G[u+1][v] == 1 and color[u+1][v] == 'w':
            elements.append(v)
            elements = DFS_Visit(G,u+1,v,color,par,elements)
    except IndexError:
        None
    try:
        if G[u][v+1] == 1 and color[u][v+1] == 'w':
            elements.append(v)
            elements = DFS_Visit(G,u,v+1,color,par,elements)
    except IndexError:
        None
    color[u][v] = 'b'
    return elements

def BFS(G,start):
    n = len(G)
    discovered = [0] * n
    discovered[start] = 1
    q = []
    elements = [start]
    q.append(start)
    while len(q) != 0:
        u = q[0]
        q.remove(u)
        for v in range(n):
            if G[u][v] == 1:
                if discovered[v] == 0:
                    discovered[v] = 1
                    q.append(v)
                    elements.append(v)
    return elements

def BFS_Whole(G,start):
    n = len(G)
    disconnected = []
    result = []
    for i in range(n):
        elements = BFS(G,i)
        if len(elements) > 1:
            disconnected.append(elements)
    for i in range(len(disconnected)):
        connect = sorted(disconnected[i])
        if connect not in result:
            result.append(connect)
    return len(result)

def map2graph(map):
    n = len(map) * len(map[0])
    A = []
    for i in range(len(map)):
        for j in range(len(map[0])):
            current = i*len(map[0])+j
            if map[i][j] == 1:
                if i-1>=0:
                    if map[i-1][j] == 1:
                        next = (i-1)*len(map[0])+j
                        A.append((current,next))
                if j-1>=0:
                    if map[i][j-1] == 1:
                        next = i*len(map[0])+j-1
                        A.append((current,next))
                if i+1<len(map):
                    if map[i+1][j] == 1:
                        next = (i+1)*len(map[0])+j
                        A.append((current,next))
                if j+1<len(map[0]):
                    if map[i][j+1] == 1:
                        next = i*len(map[0])+j+1
                        A.append((current,next))
    return A,n


map = [[1, 1, 0],
[1, 0, 1],
[1, 0, 1]]
A,n = map2graph(map)
#A = [(0,1),(0,3),(1,0),(3,0),(3,6),(5,8),(6,3),(8,5)]
G = UDG(A,n)

r = BFS_Whole(G,0)
print(r)
#r = bfs(map, 0, 0)
#print(r)
