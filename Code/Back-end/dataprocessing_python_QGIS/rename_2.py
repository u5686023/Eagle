import os
import shutil

# defaults write com.apple.screencapture location ~/Desktop
def move(src, dest):
    shutil.move(src, dest)

train_path = '~/Desktop/train_5.txt'
final_path = '~/Documents/COMP3500/8047-8475/'
image_path = '~/Desktop/screenshots/'

with open(os.path.expanduser(train_path),'r') as f:
    content = f.read().splitlines()
content = [x.strip() for x in content]
f.close()

ptr = 1
for i in range(len(content)):
    line = content[i].rstrip()
    if i % 2 == 0:
        if line:
            image_name = line
            for filename in os.listdir(os.path.expanduser(image_path)):
                if filename.startswith("rename_"+str(ptr)+".png"):
                    image_src = os.path.expanduser(image_path+filename)
                    image_des = os.path.expanduser(image_path+image_name+".jpg")
                    os.rename(image_src, image_des)

                    folder = line[:len(line)-2]
                    dirName = os.path.expanduser(final_path+folder)
                    if not os.path.exists(dirName):
                        os.mkdir(dirName)

                    final_des = os.path.expanduser(dirName+'/'+image_name+".jpg")
                    shutil.move(image_des, final_des)
            ptr = ptr +1