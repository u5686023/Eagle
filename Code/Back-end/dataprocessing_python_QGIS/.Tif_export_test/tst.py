# How to export .tif from raster image
# https://gis.stackexchange.com/questions/131160/load-the-raster-image-which-is-in-tif-format-using-qgis-api
#
# Load shape file 
# Copyright: The Geospatial Desktop
# http://geospatialdesktop.com/2009/02/creating_a_standalone_gis_application_1/  
#
# Load raster image
# Copyright: QGIS
# https://docs.qgis.org/testing/en/docs/pyqgis_developer_cookbook/loadlayer.html#raster-layers


import osgeo.gdal
from  gdalconst import *
from PyQt4.QtCore import QFileInfo,QSettings
from qgis.core import QgsRasterLayer, QgsCoordinateReferenceSystem

# load shape file
file = QFileDialog.getOpenFileName(self, "Open TIF Image", ".", "Tagged image (*.tif)")
fileInfo = QFileInfo(file)    
fileName = fileInfo.fileName()
baseName = fileInfo.baseName()

# load raster image in qgis
rlayer = QgsRasterLayer(file, baseName)
if not rlayer.isValid():
    print ("Layer failed to load")