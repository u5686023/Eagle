import os
from  osgeo import ogr, osr
#driver = ogr.GetDriverByName('ESRI Shapefile')
#shp = driver.Open('block.shp')

# Get Projection from layer
#layer = shp.GetLayer()
#spatialRef = layer.GetSpatialRef()
#print (spatialRef)

daShapefile = r"C:\Temp\Voting_Centers_and_Ballot_Sites.shp"

driver = ogr.GetDriverByName('ESRI Shapefile')

dataSource = driver.Open(daShapefile, 0) # 0 means read-only. 1 means writeable.

# Check to see if shapefile is found.
if dataSource is None:
    print ('Could not open')
else:
    print ('Opened')
    layer = dataSource.GetLayer()
    featureCount = layer.GetFeatureCount()
    print ("Number of features in " + os.path.basename(daShapefile)+" : "+featureCount)


#layer = iface.activeLayer()

layer = QgsMapLayerRegistry.instance().mapLayersByName('block')[0]

new_path = '~/Desktop/block_id.txt'
with open(os.path.expanduser(new_path),'w') as f:
    for l in layer.getFeatures():
        f.write(str(l['Id'])+'\n')
f.close()