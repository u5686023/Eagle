import os

root = "/Users/kira/Desktop"

ID_path = root + "/id_done.txt"

root_dir = os.path.dirname(__file__)
print(root_dir)

images_dir = root_dir + "/Clipping_test"

# *** get the Ids from the ID txt *** #

def read_IDs(ID_path):
    with open(ID_path) as f:
        content = f.read().splitlines()
    content = [x.strip() for x in content]
    return content
    # print(content)
    # print(len(content))
    f.close()

IDs = read_IDs(ID_path)

IDs2 = []

no = 0
for i in range(0, len(IDs)):
    if no % 2 == 0:
        IDs2.append(IDs[i])
    no += 1

IDs = IDs2
print(len(IDs))


# *** get all the Screenshots automatically *** #

images = []

for r, dirs, files in os.walk(root):
    for file in files:
        file_name = file
        if file_name[0:11] == "Screen Shot":
            images.append(file_name)
        # print(file_name[0:11])


for i in range(0, len(images)):
    words = images[i].split()
    for j in range(0, len(words)-1):
        str = '\\'
        words[j] = words[j] + str
    # print(words)
    new_name = ""
    for word in words:
        new_name = new_name + word + " "
    images[i] = new_name

images.sort()
print(len(images))


def select_id(ids):
    result = ""
    for i in range(0, len(ids)):
        if ids[i] != "_":
            result += ids[i]
        else:
            return result


for i in range(0, len(images)):
    original = root + "/" + images[i]
    # print(original)
    new_name = images_dir + "/" + select_id(IDs[i]) + "/" +  IDs[i]
    # print(root)
    # print(new_name)
    # print("mv " + original + " " + new_name+".jpg")
    os.system(
        "mv " + original + " " + new_name+".jpg"
    )


