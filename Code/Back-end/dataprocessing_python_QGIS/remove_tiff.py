import os
from osgeo import gdal, ogr


def read_IDs(ID_path):
    with open(ID_path) as f:
        content = f.read().splitlines()
    content = [x.strip() for x in content]
    f.close()
    return content

root_dir = os.path.dirname(__file__)
image_dir = root_dir+"/Clipping_test"
print(root_dir)

ID_path = root_dir + "/demoID.txt"

IDs = read_IDs(ID_path)
IDs.sort()

# print(IDs)

for id in IDs:
    file_dir = image_dir + "/" + id
    print(file_dir)

    for root, dirs, files in os.walk(file_dir):
        # print(files)
        for file in files:
            if str(file) != "block.jpg" and str(file) != "block_2.jpg":
                # print(file)
                f_path = file_dir + "/" + str(file)
                os.system(
                    "rm " + f_path
                )


    # input_path = file_dir + "/pm25_2.tif"
    # output_path = file_dir + "/block_2.jpg"
    # convert_tif_to_jpg(input_path, output_path)