import os
import shutil
import random

# defaults write com.apple.screencapture location ~/Desktop
def move(src, dest):
    shutil.move(src, dest)

input_path = '~/Desktop/tst.csv'
output_path = '~/Desktop/new_tst.csv'
att_path = '~/Desktop/att.txt'

with open(os.path.expanduser(input_path),'r') as f:
    content = f.read().splitlines()
content = [x.strip() for x in content]
f.close()

attribute = {}
with open(os.path.expanduser(att_path),'r') as f:
    atts = f.read().splitlines()
atts = [x.strip() for x in atts]
f.close()
for i in range(0,len(atts)-1,2):
    id = atts[i].rstrip()
    att = atts[i+1].rstrip()
    attribute[id] = att

with open(os.path.expanduser(output_path),'w') as f:
    opening = content[0].rstrip()+", Attribute"
    f.write(opening+'\n')
    for i in range(1,len(content)):
        line = content[i].rstrip()
        if line:
            fid = line.split(",")[0]
            line = line + ", " + str(random.randint(0,10))
            """
            if fid in attribute:            
                line = line + ", " + attribute[fid]
            else:
                line = line + ", 0"
            """
            f.write(line+'\n')
f.close()