import os

read_path = '~/Desktop/act5_id.txt'
new_path = '~/Desktop/new_train.txt'
with open(os.path.expanduser(read_path),'r') as f:
    lines = sorted(f.read().splitlines())
f.close()

ids = []
for l in lines:
    ids.append(int(l))
ids.sort()

with open(os.path.expanduser(new_path),'w') as f:
    for id in ids:
        if id >= 8047 and id <= 8475:
            #f.write(str(id)+'\n')
            f.write(str(id)+"_1"'\n'+'\n')
            f.write(str(id)+"_2"'\n'+'\n')
            #f.write(str(id)+"_3"'\n'+'\n')
f.close()