# references : https://www.youtube.com/watch?v=K8eRbeBrHHk
import os
from osgeo import gdal, ogr

demoIDs = {"30047", "184397", "197530"}
root_dir = os.path.dirname(__file__)
raster_input_path = root_dir + "/act6.tif"
print(raster_input_path)

ID_path = root_dir + "/demoID.txt"
def read_IDs(ID_path):
    with open(ID_path) as f:
        content = f.read().splitlines()
    content = [x.strip() for x in content]
    return content
    # print(content)
    # print(len(content))
    f.close()


IDs = read_IDs(ID_path)
IDs.sort()
# for i in range(0, len(IDs)):
#     IDs[i] = int(IDs[i])

print(IDs)

# IDs.sort()
# print(IDs)
# no = 0
# for id in IDs:
#     if no % 100 == 0:
#         print(id)
#     no += 1
# print(IDs)
# print(len(IDs))


def get_IDs():
    shape_dir = root_dir + "/Clipping_test"
    IDs = []
    for root, dirs, files in os.walk(shape_dir):
        for dir in dirs:
            IDs.append(dir)
    return IDs


def convert_tif_to_jpg(input_path, output_path):
    s = "gdal_translate -ot Float32 -of JPEG " + input_path + " " + output_path
    print(s)
    os.system(
        "gdal_translate -ot Float32 -of JPEG " + input_path + " " + output_path
    )


def ClipRasterwithpolygon(RasterPath, PolygonPath, OutputPath):
    os.system(
        "gdalwarp -dstnodata -q -cutline " + PolygonPath + " -crop_to_cutline " + " -of GTiff " + RasterPath + " " + OutputPath)


def createclippingpolygons(inpath, field):
    inpath = root_dir + "/" + inpath
    print("path", inpath)
    driverSHP = ogr.GetDriverByName("Esri Shapefile")
    ds = driverSHP.Open(inpath)
    if ds is None:
        print ('layer not open')
    lyr=ds.GetLayer()
    SpatialRef= lyr.GetSpatialRef()

    # block_no = 0

    for feature in lyr:
        # if block_no >= 5:
        #     exit()
        # print(feature[field])
        # block_no +=1
        # if feature['OBJECTID'] == 122811:
        # print("find it!")
        fieldVal = feature.GetField(field)
        print(fieldVal)
        if str(fieldVal) in IDs:
            print(fieldVal, "the block is in demo range")
            os.mkdir("Clipping_test/" + str(fieldVal))
            outds = driverSHP.CreateDataSource("Clipping_test/" + str(fieldVal) + "/clip.shp")
            outlyr = outds.CreateLayer(str(fieldVal) + "/clip.shp", srs=SpatialRef, geom_type=ogr.wkbPolygon)
            OutDfn = outlyr.GetLayerDefn()
            ingeom = feature.GetGeometryRef()
            outFeat = ogr.Feature(OutDfn)
            outFeat.SetGeometry(ingeom)
            outlyr.CreateFeature(outFeat)
            # exit()


def ClipRasters(inpath, field):
    print("clip inpath", inpath)
    # determine all the IDs
    IDs = get_IDs()
    # IDs.sort()
    # print(IDs)

    # clip the raster according to the shapefile we have clipped
    driverSHP= ogr.GetDriverByName("Esri Shapefile")
    ds= driverSHP.Open(inpath)
    if ds is None :
        print ('layer not open')
    lyr= ds.GetLayer()
    for feature in lyr:
        # check if we have already clipped the shapefile of this block
        # print(feature[field])

        # print(feature[field] in IDs)
        if str(feature[field]) in IDs:
            # print("success")
            # exit()

        # if feature['OBJECTID'] == 122811:
            fieldVal= feature.GetField(field)
            print(fieldVal)
            print("Clipping_test/" +str(fieldVal)+"/pm25_2.tif")
            ClipRasterwithpolygon("act7.tif","Clipping_test/" +str(fieldVal),"Clipping_test/" +str(fieldVal)+"/pm25_2.tif")
            print("clip it")
            # exit()


def convert_tif_to_jpg_mul():
    # print(get_IDs())
    test_dir = root_dir + "/Clipping_test"
    print(test_dir)
    for id in IDs:
        file_dir = test_dir+"/"+id
        # print(file_dir)
        input_path = file_dir+"/pm25_2.tif"
        output_path = file_dir+"/block_2.jpg"
        convert_tif_to_jpg(input_path, output_path)


# run
# os.mkdir("Clipping_test")
# createclippingpolygons("ACT_Blocks/ACT_Blocks.shp", "ID")
# ClipRasters("ACT_Blocks/ACT_Blocks.shp", "ID")
convert_tif_to_jpg_mul()


print("root prints out directories only from what you specified")
print("dirs prints out sub-directories from root")
print("files prints out all files from root and directories")
print("*" * 20)


# print(get_IDs())

# for root, dirs, files in os.walk("/Users/kira/Documents/study/2018-2/comp3500/Eagle2/Eagle/Code/Back-end/dataprocessing_python_QGIS/Clipping_test"):
#     # print("look")
#     # print(root)
#     # print(dirs)
#     for dir in dirs:
#         print(dir)
#         print(dir == "6778")
#     # print(files)