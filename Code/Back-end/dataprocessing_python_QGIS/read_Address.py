import os
from osgeo import gdal, ogr


def read_IDs(ID_path):
    with open(ID_path) as f:
        content = f.read().splitlines()
    content = [x.strip() for x in content]
    f.close()
    return content

root_dir = os.path.dirname(__file__)
ID_path = root_dir + "/demoID.txt"

IDs = read_IDs(ID_path)
IDs.sort()

shp_path = root_dir + "/ACT_Blocks/ACT_Blocks.shp"

print(IDs)
print(len(IDs))

all_IDs = []

driverSHP = ogr.GetDriverByName("Esri Shapefile")
ds = driverSHP.Open(shp_path)
if ds is None:
    print('layer not open')
lyr = ds.GetLayer()
SpatialRef = lyr.GetSpatialRef()

f = open('id_address.txt','w+')

for feature in lyr:
    fieldVal = feature.GetField("ID")
    all_IDs.append(str(fieldVal))
    # print(fieldVal)

    if str(fieldVal) in IDs:
        address = feature.GetField("ADDRESSES")
        if address is None:
            address = "None"
        s1 = str(fieldVal) + "\n"
        s2 = address + "\n"
        print(fieldVal)
        f.write(s1)
        f.write(s2)

f.close()

all_IDs.sort()
# print(all_IDs)
print(len(all_IDs))

for id in IDs:
    if id not in all_IDs:
        print("error")
        exit()
print("true")