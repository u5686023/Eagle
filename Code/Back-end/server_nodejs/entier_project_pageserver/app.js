
//the basic express server
var express = require('express');
var app = express();
//execute the python process
var exec = require('child_process').exec;
var py_name = 'img';
//watch the file change
var fs = require('fs');
var watch = 'page';

//set the static dir
app.use(express.static('public'));
app.use(express.static(__dirname + '/page'));

//set the router
app.get('/',function (req,res) {
    console.log(__dirname + '/page/map1.html');
    res.sendfile(__dirname + '/page/map1.html'  ,function (err) {
        if(err) res.sendStatus(404);
        else console.log('loading map');
    });
});
app.get('/img_process',function (req,res) {

    console.log('the process is beginning');

    //execute the python file
    exec('python python/imgprocess_show.py',function (err, stdout) {
    if(err){
        console.log('stderr ',err);
    }
    if(stdout){
        console.log('stdout',stdout);
    }
    });

    res.send('The python program')

    /*
    //when the result img is generated
    //send the webpage using the result
    var flag = 1;
    fs.watch(watch,function (event) {
        console.log('event is ',event,'time',flag);
        if(flag == 1){
            res.sendfile(__dirname + '/page/exp1.html',function (err) {
                if(err) {
                    res.sendStatus(404);
                    console.log(err);
                }
                else console.log('processing');
            });
            flag += 1
        }
    });
    */
});

//listen the port and start the server
var server = app.listen(8081,function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log("应用实例，访问地址为 http://%s:%s", host, port)
});