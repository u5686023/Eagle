import cv2


# read the images and transfer to gray-img, medianblur
# @imgname the path of image
# @meblursize the parameter of the medianblur
# @output the output original BGR-image
# @gray_img the output gray-image
def read(imgname, meblursize):
    output = cv2.imread(imgname)
    output = cv2.resize(output, (400, 400))
    output = cv2.medianBlur(output, meblursize)
    gray_img = cv2.cvtColor(output, cv2.COLOR_BGR2GRAY)
    return output, gray_img


# check the neighbourhood of a Canny-image to detect the differences between two images
# @a the former image
# @b the later image
def detect(imga, imgb):
    result = imgb.copy()
    for i in range(1, imga.shape[1] - 1):
        for j in range(1, imga.shape[0] - 1):
            if imgb[i, j]:
                for p in range(-1, 2):
                    for k in range(-1, 2):
                        if imga[i+p, j+k]:
                            result[i, j] = 0

    return result


extra_img = []
# extra the ROI and export the cell images
def extra(org2, x1, y2, y1, x2):
    cell = org2[int(x1):int(x2), int(y1):int(y2)]
    cell = cv2.resize(cell, (100, 100))
    extra_img.append(cell)


# calculate the area of each cell
# @dt detection result
# @x1,y1 the beginning point of the target cell
# @x2,y2 the final point of the target cell
def count(dt, x1, y2, y1, x2):
    c = 0
    for i in range(int(x1), int(x2)):
        for j in range(int(y1), int(y2)):
            if dt[i, j]:
                c = c + 1
    return c


# draw the original image respect to the detection result in line
# @org target image
# @canny detection resul
# @color 0 blue,1 red
def draw(org, canny, color):
    if color:
        for i in range(0, canny.shape[1]):
            for j in range(0, canny.shape[0]):
                if canny[i, j]:
                    org[i, j, 0] = 0
                    org[i, j, 1] = 0
                    org[i, j, 2] = 255
    else:
        for i in range(0, canny.shape[1]):
            for j in range(0, canny.shape[0]):
                if canny[i, j]:
                    org[i, j, 0] = 255
                    org[i, j, 1] = 0
                    org[i, j, 2] = 0


# draw the original image with valid cells
# @org the target image
# @x1,y1 coordinate of the lefttop
# @x2,y2 coordinate of the rightbottom
# color: 1-blue 2-red
def draw_unit(org, x1, y2, y1, x2, color):
    drawimg = org.copy()
    if color == 1:
        scolor = (255, 0, 0)
    else:
        scolor = (0, 0, 255)
    cv2.rectangle(drawimg, (int(y1), int(x1)), (int(y2), int(x2)), scolor, 2)
    return drawimg


# count in the image cells
# @dt the detection result
# @org1 the former image used to paint the valid cell
# @org2 the later image used to extract the ROI
# @grid the area of cell is grid*grid
# @thresh the thresh of valid area in each cell
def cell_present(dt, org1, org2, grid, thresh):
    org2_copy = org2.copy()

    a = dt.shape[1] / grid
    b = dt.shape[0] / grid

    flag = 1
    for i in range(0, grid):
        for j in range(0, grid):
            c = count(dt, 0 + b * i, dt.shape[1] / grid + a * j, 0 + a * j, dt.shape[0] / grid + b * i)
            if c > thresh:
                org1 = draw_unit(org1, 0 + b * i, dt.shape[1] / grid + a * j, 0 + a * j, dt.shape[0] / grid + b * i, 1)
                # extract one cell as example and paint the relative area in org2
                if flag < 4:
                    extra(org2_copy, 0 + b * i, dt.shape[1] / grid + a * j, 0 + a * j, dt.shape[0] / grid + b * i)
                    org2 = draw_unit(org2, 0 + b * i, dt.shape[1] / grid + a * j, 0 + a * j, dt.shape[0] / grid + b * i, 2)
                    flag = flag + 1
                    print('aaaaaa', flag)
    return org1, org2


# function for createTrackBar
# no real meaning
def nothing(x):
    pass


# the entrance function
def img_process():

    # step1 read the images
    org1, gray_img1 = read('a1.jpg', 3)
    org2, gray_img2 = read('a2.jpg', 3)

    # step2 get the canny image and use them to detect
    can1 = cv2.Canny(org1, 100, 300)
    can2 = cv2.Canny(org2, 100, 300)
    re = detect(can1, can2)

    # step3 draw and present by cell
    # org1_pnt painted former image
    # org2_pnt painted later image
    org1_pnt, org2_pnt = cell_present(re, org1, org2, 20, 50)
    cv2.imwrite('img_result/former.jpg', org1_pnt)
    cv2.imwrite('img_result/later.jpg', org2_pnt)
    cv2.imwrite('img_result/detect_rst.jpg', re)
    for i in range(0, 3):
        cv2.imwrite('img_result/cell' + str(i) + '.jpg', extra_img[i])

img_process()
