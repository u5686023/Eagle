import cv2

img = cv2.imread('page/cat.jpg')
img = cv2.resize(img, (400,400))

#cv2.imshow('image', img)

canny = cv2.Canny(img, 10, 150)
#cv2.imshow('canny', canny)

cv2.imwrite('page/result.jpg', canny)
cv2.imwrite('page/start.jpg', img)
cv2.waitKey(0)
