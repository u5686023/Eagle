To set up the open-cv python environment, the relevant source is:
http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_setup/py_setup_in_windows/py_setup_in_windows.html

To set up visual studio 2015 environment, the relevant source is:
http://funvision.blogspot.com.au/2015/11/install-opencv-visual-studio-2015.html

To set up gdal environment, the relevant source is:
https://sandbox.idre.ucla.edu/sandbox/general/how-to-install-and-run-gdal.html