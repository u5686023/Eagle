import time
import numpy as np
import cv2


# ********* 2018.7.11 *************
# ******* flexible contour ********

# ********* 2018.9.15 *************
# *** add write func ***

write_index = 0

# read the images and transfer to gray-img, medianblur
# @imgname the path of image
# @meblursize the parameter of the medianblur
# @output the output original BGR-image
# @gray_img the output gray-image
def read(imgname, meblursize):
    output = cv2.imread(imgname)
    output = cv2.resize(output, (400, 400))
    output = cv2.medianBlur(output, meblursize)
    gray_img = cv2.cvtColor(output, cv2.COLOR_BGR2GRAY)
    return output, gray_img


# create a record map to mark the selected status of grid cell
# @img the original image
# @grid the size of the square grid
def gridmap(img, grid):

    column = img.shape[0] // grid
    row = img.shape[1] // grid
    map = np.zeros((column, row))
    return map


# check the neighbourhood of a Canny-image to detect the differences between two images
# @a the former image
# @b the later image
def detect(imga, imgb):
    result = imgb.copy()
    for i in range(1, imga.shape[1] - 1):
        for j in range(1, imga.shape[0] - 1):
            if imgb[i, j]:
                if np.sum(imga[i-1:i+1, j-1:j+1]):
                    result[i, j] = 0

    return result


# calculate and return the marked grid cells as draw
# meanwhile return the projection map of the marked cells
# @ detect: the result of the difference detection
# @ grid: the size of the grid cell
# @ map: the all-zeros projection map of the grid board
def cellpresent(detect, grid, map):

    # create the marked grid cells board
    draw = np.zeros((detect.shape[0], detect.shape[1], 3), np.uint8)

    i = 0
    c1 = 0
    while i <= detect.shape[0] - grid:
        j = 0
        c2 = 0
        while j <= detect.shape[1] - grid:
            c = detect[i:i+grid, j:j+grid].sum() / 255
            if c >= 50:
                draw = cv2.rectangle(draw, (j, i), (j + grid, i + grid), (0, 0, 255), -1)
                map[c1, c2] = 1
            j += grid
            c2 += 1

        i += grid
        c1 += 1

    return draw, map


# extract the ROI and export the connected areas images
# @ img_overlap: the image used to represent the marked areas
# @ img_later: the later original image used to be the recourse of the extraction
# @ draw: the image of filled grid cell
def extract(img_overlap, img_later, draw, org2_name=None):

    global write_index
    img_later_copy = img_later.copy()

    # ***step1*** pre-process the draw
    # erode in order to distinguish the areas better
    kernel = np.ones((3, 3), np.uint8)
    draw_erode = cv2.erode(draw, kernel)
    draw_gray = cv2.cvtColor(draw_erode, cv2.COLOR_BGR2GRAY)

    # ***step2*** find the contours of the connected areas
    im2, contour, hierarchy = cv2.findContours(draw_gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # calculate vertical rct and draw the rotated bounding rectangle of each contours
    x, y, w, h = (0, 0, 0, 0)
    contour_record = []  # record the contours waited for being ordered by area size
    area_index = []  # record the area size of the connected areas and their contour index
    for i in range(len(contour)):
        # ***step3*** calculate the vertical rectangle bounding
        x, y, w, h = cv2.boundingRect(contour[i])
        area = w * h
        contour_record.append([x, y, w, h])
        area_index.append([area, i])

        # ***step4*** draw the rotated rectangle bounding
        rct = cv2.minAreaRect(contour[i])
        box = np.int0(cv2.boxPoints(rct))
        cv2.drawContours(img_overlap, [box], 0, (0, 255, 0))

    # ***step5*** extract w.r.t vertical rct from the later image
    if len(contour):
        # order by the area size
        area_order = np.array(sorted(area_index))
        # chose the 4 largest areas and represent
        index_order = area_order[-4:, 1]
        print(index_order)
        k = 0
        for i in index_order:
            # fetch the points of the objects' contours
            x, y, w, h = contour_record[i]
            # clip the objects on the original image based on the points
            etr = img_later_copy[y:y + h, x:x + w]
            # draw the boundary on the drown image
            cv2.rectangle(img_later, (x, y), (x + w, y + h), (255, 0, 0), 2)
            # if the size of the object is too small to draw correctly then resize it
            if w < 150:
                etr = cv2.resize(etr, (150, int(h * 150/w)))
            # show the objects on the new window
            cv2.imshow('extract' + str(k), etr)

            # save the objects into the local folder and be made into the training dataset
            write_path = 'clips/' + org2_name + str(write_index) +str(i) + '.jpg'
            cv2.imwrite(write_path, etr)

            # increase the window index
            k += 1
            print(write_path)

    return img_overlap


# function for createTrackBar
# no real meaning
def nothing(x):
    pass


# the entrance function
def imgprocess():

    grid = 20
    thresh = 50
    org1_path = 'e1.jpg'
    org2_path = 'e2.jpg'
    global write_index

    # ***step1*** read the images
    org1, gray_img1 = read('img1/' + org1_path, 3)
    org2, gray_img2 = read('img1/' + org2_path, 3)
    # create control bar of the canny
    cv2.namedWindow('img', 0)
    cv2.resizeWindow('img', org1.shape[0], org1.shape[1])
    cv2.namedWindow("control", 0)
    cv2.createTrackbar("lowc", "control", 0, 1000, nothing)
    cv2.createTrackbar("highc", "control", 0, 1000, nothing)
    # create the showing window
    cv2.createTrackbar("image_select", "control", 1, 3, nothing)

    lowc_pre = cv2.getTrackbarPos('lowc', 'control')  # 100,300
    highc_pre = cv2.getTrackbarPos('highc', 'control')

    org1_copy = org1.copy()
    org2_copy = org2.copy()
    while True:

        tmstart = time.clock()  # record the running time

        # ***step2*** get the canny image and use them to detect
        org1 = org1_copy.copy()
        org2 = org2_copy.copy()
        lowc = cv2.getTrackbarPos('lowc', 'control')  # 100,300
        highc = cv2.getTrackbarPos('highc', 'control')
        flag = cv2.getTrackbarPos('image_select', 'control')

        # for writing image
        # if the parameter is changed then update the writen index
        if lowc != lowc_pre or highc != highc_pre:
            write_index += 1
            lowc_pre = lowc
            highc_pre = highc

        can1 = cv2.Canny(org1, lowc, highc)
        can2 = cv2.Canny(org2, lowc, highc)
        detc = detect(can1, can2)

        # ***step3*** draw and present w.r.t the detection result
        map = gridmap(detc, grid)
        draw, _ = cellpresent(detc, grid, map)
        overlap = cv2.addWeighted(org1, 0.8, draw, 1, 0)

        # ***step4*** extract the connected areas
        overlap = extract(overlap, org2, draw, org2_path[0])

        # ***step5*** show the result
        if flag == 0:
            cv2.imshow("img", org1_copy)
        elif flag == 1:
            cv2.imshow("img", org2)
        elif flag == 2:
            cv2.imshow("img", overlap)
        elif flag == 3:
            cv2.imshow("img", detc)

        cv2.waitKey(10)

        print('running time: ', time.clock() - tmstart)



imgprocess()
