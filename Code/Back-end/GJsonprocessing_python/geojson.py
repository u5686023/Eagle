# geojson file by python API
import json
from osgeo import ogr

# load geojson file
with open('Crystal.geojson') as file:
    data = json.load(file)

# extract coordinates for each blocks and form polygons
for feature in data['features']:
    #print (feature['geometry']['type'])
    #print (feature['geometry']['coordinates'])
    # add points into a ring
    ring = ogr.Geometry(ogr.wkbLinearRing)
    for point in feature['geometry']['coordinates']:
        x = point[0]
        y = point[1]
        #print x,y
        ring.AddPoint(x,y)
    # Add to the polygon
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)
    #print json.dumps(polygon)
