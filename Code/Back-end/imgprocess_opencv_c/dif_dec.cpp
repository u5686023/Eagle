#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include <vector>
#include <iostream>

using namespace std;
using namespace cv;

//2gray, medianblur
void read(string name, Mat& org, Mat&gray, int meblursize) {
	org = imread(name);
	resize(org, org, Size(600, 600));
	medianBlur(org, org, meblursize);
	cvtColor(org, gray, CV_RGB2GRAY);
}

Mat deduct(Mat a,Mat b) {
	Mat re = a - b;
	return re;
}

//a as the former b as later
Mat deduct2(Mat a, Mat b) {
	Mat result;
	b.copyTo(result);
	for (int i = 1; i < a.rows - 2; i++) {
		for (int j = 1; j < a.cols - 2; j++) {
			if (b.at<uchar>(i, j)) {
				for (int p = -1; p <= 1; p++) {
					for (int k = -1; k <= 1; k++) {
						if (a.at<uchar>(i + p, j + k)) {
							result.at<uchar>(i, j) = 0;
						}
					}
				}
			}
		}
	}
	return result;
}

void draw(Mat &org, Mat canny, int flag) {
	if (flag == 0) {
		for (int i = 0; i < canny.rows; i++) {
			for (int j = 0; j < canny.cols; j++) {
				if (canny.at<uchar>(i, j) != 0) {
					org.at<Vec3b>(i, j)[0] = 255;
					org.at<Vec3b>(i, j)[1] = 0;
					org.at<Vec3b>(i, j)[2] = 0;
				}
			}
		}
	}
	if (flag == 1) {
		for (int i = 0; i < canny.rows; i++) {
			for (int j = 0; j < canny.cols; j++) {
				if (canny.at<uchar>(i, j) != 0) {
					org.at<Vec3b>(i, j)[0] = 0;
					org.at<Vec3b>(i, j)[1] = 0;
					org.at<Vec3b>(i, j)[2] = 255;
				}
			}
		}
	}
}


int main() {

	Mat org1, gray1;
	read("1.jpg", org1, gray1, 3);
	Mat org2, gray2;
	read("2.jpg", org2, gray2, 3);

	int lowc = 200;
	int highc = 150;

	namedWindow("control",0);
	createTrackbar("lowc", "control", &lowc, 1000);
	createTrackbar("highc", "control", &highc, 1000);

	while (1) {
		Mat can1;
		Canny(gray1, can1, lowc, highc);
		Mat can2;
		Canny(gray2, can2, lowc, highc);

		Mat dt = deduct2(can1, can2);

		draw(org1, dt, 1);
		imshow("org1", org1);
//		imshow("canny1", can1);
		imshow("org2", org2);
//		imshow("canny2", can2);
		imshow("deduct", dt);

		waitKey(30);
	}
}

/*appling*/