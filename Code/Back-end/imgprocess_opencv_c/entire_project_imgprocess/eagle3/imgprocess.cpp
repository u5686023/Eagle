#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/imgcodecs.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/ml.hpp>
#include <iostream>

using namespace std;
using namespace cv;

//read the images and transfer to gray-img, medianblur
//@name the path of image
//@output the output original BGR-image
//@gray the output gray-image
//@meblursize the parameter of the medianblur
void read(string name, Mat& output, Mat&gray, int meblursize) {
	output = imread(name);
	resize(output, output, Size(800, 800));
	medianBlur(output, output, meblursize);
	cvtColor(output, gray, CV_RGB2GRAY);
}

//check the neighbourhood of a Canny-image to detect the differences between two images  
//@a the former image
//@b the later image
Mat detect(Mat a, Mat b) {
	Mat result;
	b.copyTo(result);
	for (int i = 1; i < a.rows - 2; i++) {
		for (int j = 1; j < a.cols - 2; j++) {
			if (b.at<uchar>(i, j)) {
				for (int p = -1; p <= 1; p++) {
					for (int k = -1; k <= 1; k++) {
						if (a.at<uchar>(i + p, j + k)) {
							result.at<uchar>(i, j) = 0;
						}
					}
				}
			}
		}
	}
	return result;
}

//extra the ROI and export the cell images
Mat extra(Mat org, int x1, int y1, int x2, int y2) {
	Rect rec(x1, y1, x2, y2);
	Mat cell = org(rec);
	resize(cell, cell, Size(100, 100));

	Mat canny;
	Canny(cell, canny, 150, 50);
	imshow("cellcanny", canny);
	imshow("cell", cell);
	return cell;
}

//calculate the valid area of each cell
//@dt detection resul
//@x1,y1 the beginning point of the target cell
//@x2,y2 the final point of the target cell
int count(Mat&dt, int x1, int y2, int y1, int x2) {
	int c = 0;
	for (int i = x1; i<x2; i++) {
		for (int j = y1; j<y2; j++) {
			if (dt.at<uchar>(i, j) != 0) {
				c++;
			}
		}
	}

	return c;
}

//draw the original image respect to the detection result with line
//@org target image
//@canny detection resul
//@color 0 blue,1 red
void draw(Mat &org, Mat canny, int color) {
	if (color == 0) {
		for (int i = 0; i < canny.rows; i++) {
			for (int j = 0; j < canny.cols; j++) {
				if (canny.at<uchar>(i, j) != 0) {
					org.at<Vec3b>(i, j)[0] = 255;
					org.at<Vec3b>(i, j)[1] = 0;
					org.at<Vec3b>(i, j)[2] = 0;
				}
			}
		}
	}
	if (color == 1) {
		for (int i = 0; i < canny.rows; i++) {
			for (int j = 0; j < canny.cols; j++) {
				if (canny.at<uchar>(i, j) != 0) {
					org.at<Vec3b>(i, j)[0] = 0;
					org.at<Vec3b>(i, j)[1] = 0;
					org.at<Vec3b>(i, j)[2] = 255;
				}
			}
		}
	}
}

//draw the original image with valid cells 
//@org the target image
//@x1,y1 coordinate of the lefttop
//@x2,y2 coordinate of the rightbottom
void drawunit(Mat &org, int x1, int y2, int y1, int x2) {
	rectangle(org, Point(y1, x1), Point(y2, x2), Scalar(255, 0, 0), 2);
}

//count in the image cells
//@dt the dectection result
//@org1 the former image
//@grid the area of cell is grid*grid
//@thresh the thresh of valid area in each cell
void cellcount(Mat&dt,Mat&org1,int grid,int thresh) {
	int a = dt.rows / grid;
	int b = dt.cols / grid;

	int flag = 1;
	for (int i = 0; i<grid; i++) {
		for (int j = 0; j<grid; j++) {
			int c = count(dt, 0 + b*(i), dt.rows / grid + a*(j), 0 + a*(j), dt.cols / grid + b*(i));
			if (c > thresh) {
				drawunit(org1, 0 + b*(i), dt.rows / grid + a*(j), 0 + a*(j), dt.cols / grid + b*(i));
				if (flag < 2) {
					extra(org1, 0 + b*(i), dt.rows / grid + a*(j), 0 + a*(j), dt.cols / grid + b*(i));
					flag++;
				}
			}
		}
	}

}


int main() {

	//read fomer and later images
	Mat  org1, gray1;
	read("1.jpg", org1, gray1, 3);
	Mat org2, gray2;
	read("2.jpg", org2, gray2, 3);
	Mat org = Mat::zeros(Size(600, 600), CV_8UC3);
	org1.copyTo(org);
	
	//create the bar to adjust the parameters of Canny
	int lowc = 200;
	int highc = 150;
	namedWindow("control", 0);
	createTrackbar("lowc", "control", &lowc, 1000);
	createTrackbar("highc", "control", &highc, 1000);

	//create bar to show different images in same window
	int flag = 0;
	namedWindow("img");
	createTrackbar("select", "control", &flag, 3);

	while (1) {

		//detection
		org.copyTo(org1);
		Mat can1;
		Canny(gray1, can1, lowc, highc);
		Mat can2;
		Canny(gray2, can2, lowc, highc);
		Mat dt = detect(can1, can2);

		//draw and presentate in cells
		cellcount(dt, org1, 40, 30);
		
		//draw the beginning image respect to the detection result by line
		draw(org1, dt, 1);

		//presentate images
		switch (flag)
		{
		case 0:imshow("img", org); break;
		case 1:imshow("img", org2); break;
		case 2:imshow("img", org1); break;
		case 3:imshow("img", dt); break;
		}
		waitKey(30);
	}
}

/*appling*/

