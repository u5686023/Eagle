# Mulong Xie
# @Australian National University

# ********* 2018.7.11 *************
# ******* flexible contour ********

# ********* 2018.9.15 *************
# *** add write func ***

# ********* 2018.10.08 *************
# *** change parameter to adapt the block images in img3 ***

# ********* 2018.10.11 *************
# *** fix parameters ***
# *** iterate whole file with interval time ***
# *** integrate NN into the iterated process ***

# *** extract the block id and the relevant address ***

import time
import numpy as np
import cv2
import os
import tensorflow as tf
write_index = 0

# read the images and transfer to gray-img, medianblur
# @imgname the path of image
# @meblursize the parameter of the medianblur
# @output the output original BGR-image
# @gray_img the output gray-image
def read(imgname, meblursize):
    output = cv2.imread(imgname)
    output = cv2.resize(output, (400, 400))
    output = cv2.medianBlur(output, meblursize)
    gray_img = cv2.cvtColor(output, cv2.COLOR_BGR2GRAY)
    return output, gray_img


# create a record map to mark the selected status of grid cell
# @img the original image
# @grid the size of the square grid
def gridmap(img, grid):

    column = img.shape[0] // grid
    row = img.shape[1] // grid
    map = np.zeros((column, row))
    return map


# check the neighbourhood of a Canny-image to detect the differences between two images
# @a the former image
# @b the later image
def detect(imga, imgb, kernel_size):
    r = kernel_size
    result = imgb.copy()
    for i in range(r, imga.shape[1] - r):
        for j in range(r, imga.shape[0] - r):
            if imgb[i, j]:
                if np.sum(imga[i-r:i+r, j-r:j+r]):
                    result[i, j] = 0

    return result


# calculate and return the marked grid cells as draw
# meanwhile return the projection map of the marked cells
# @ detect: the result of the difference detection
# @ grid: the size of the grid cell
# @ map: the all-zeros projection map of the grid board
def cellpresent(detect, grid, map, thresh):

    # create the marked grid cells board
    draw = np.zeros((detect.shape[0], detect.shape[1], 3), np.uint8)

    i = 0
    c1 = 0
    while i <= detect.shape[0] - grid:
        j = 0
        c2 = 0
        while j <= detect.shape[1] - grid:
            c = detect[i:i+grid, j:j+grid].sum() / 255
            if c >= thresh:
                draw = cv2.rectangle(draw, (j, i), (j + grid, i + grid), (0, 0, 255), -1)
                map[c1, c2] = 1
            j += grid
            c2 += 1

        i += grid
        c1 += 1

    return draw, map


# extract the ROI and export the connected areas images
# @ img_overlap: the image used to represent the marked areas
# @ img_later: the later original image used to be the recourse of the extraction
# @ draw: the image of filled grid cell
def extract(img_overlap, img_later, draw, org2_name=None):

    global write_index
    img_later_copy = img_later.copy()

    # ***step1*** pre-process the draw
    # erode in order to distinguish the areas better
    kernel = np.ones((3, 3), np.uint8)
    draw_erode = cv2.erode(draw, kernel)
    draw_gray = cv2.cvtColor(draw_erode, cv2.COLOR_BGR2GRAY)

    # ***step2*** find the contours of the connected areas
    im2, contour, hierarchy = cv2.findContours(draw_gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # calculate vertical rct and draw the rotated bounding rectangle of each contours
    x, y, w, h = (0, 0, 0, 0)
    contour_record = []  # record the contours waited for being ordered by area size
    area_index = []  # record the area size of the connected areas and their contour index
    for i in range(len(contour)):
        # ***step3*** calculate the vertical rectangle bounding
        x, y, w, h = cv2.boundingRect(contour[i])
        area = w * h
        contour_record.append([x, y, w, h])
        area_index.append([area, i])

        # ***step4*** draw the rotated rectangle bounding
        rct = cv2.minAreaRect(contour[i])
        box = np.int0(cv2.boxPoints(rct))
        cv2.drawContours(img_overlap, [box], 0, (0, 255, 0))

    etrs = []
    # ***step5*** extract w.r.t vertical rct from the later image
    if len(contour):
        # order by the area size
        area_order = np.array(sorted(area_index))
        # chose the 4 largest areas and represent
        index_order = area_order[-4:, 1]
        print(index_order)
        k = 0
        for i in index_order:
            # fetch the points of the objects' contours
            x, y, w, h = contour_record[i]
            # clip the objects on the original image based on the points
            etr = img_later_copy[y:y + h, x:x + w]
            # draw the boundary on the drown image
            cv2.rectangle(img_later, (x, y), (x + w, y + h), (255, 0, 0), 2)
            # if the size of the object is too small to draw correctly then resize it
            if w < 150:
                etr = cv2.resize(etr, (150, int(h * 150/w)))
            etrs.append(etr)

            # save the objects into the local folder and be made into the training dataset
            write_path = 'clips/' + org2_name + str(write_index) +str(i) + '.jpg'

            # increase the window index
            k += 1
            print(write_path)

    return img_overlap, etrs


# function for createTrackBar
# no real meaning
def nothing(x):
    pass


def extract_address(path='Clipping_test/id_address.txt'):
    dic = {}
    file = open(path)
    for i, line in enumerate(file):
        if i % 2 == 0:
            id = str(line[:-1])
        else:
            dic[id] = line[:-1]
    return dic


# the entrance function
def imgprocess():

    grid = 30                    # for block img : 35, 50, 0-80, 116
    thresh = 70
    address = extract_address()  # extract the address w.r.t. the block id

    with tf.Session() as sess:
        # load the pretrained model
        saver = tf.train.import_meta_graph('./trained_model/house.meta')
        saver.restore(sess, tf.train.latest_checkpoint('./trained_model'))

        graph = tf.get_default_graph()
        x_tf = graph.get_tensor_by_name('x:0')
        drop_prob_tf = graph.get_tensor_by_name('keep_drop:0')
        y_hat = graph.get_tensor_by_name('y_hat:0')
        y_pre = tf.argmax(y_hat, 1)

        list_dir = os.walk('Clipping_test')
        for root, dirs, files in list_dir:
            if root == 'Clipping_test':
                continue

            tmstart = time.clock()  # record the running time

            org1_path = os.path.join(root, 'block.jpg')
            org2_path = os.path.join(root, 'block_2.jpg')
            print(org1_path)

            # ***step1*** read the images
            org1, gray_img1 = read(org1_path, 3)
            org2, gray_img2 = read(org2_path, 3)

            # create control bar of the canny
            cv2.namedWindow('before', 0)
            cv2.namedWindow('after', 0)
            cv2.namedWindow('paint', 0)
            cv2.moveWindow('before', 0, 0)
            cv2.moveWindow('after', 400, 0)
            cv2.moveWindow('paint', 800, 0)

            lowc_pre = 10
            highc_pre = 116

            org1_copy = org1.copy()
            org2_copy = org2.copy()

            # *** step2 ** put the address on the img
            # get the block id of the block id
            id = root[14:]
            print("id " + id + address[id])
            cv2.putText(org1_copy, address[id], (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 2)

            # *** step2 *** get the canny image and use them to detect
            lowc = lowc_pre
            highc = highc_pre

            can1 = cv2.Canny(org1, lowc, highc)
            can2 = cv2.Canny(org2, lowc, highc)
            detc = detect(can1, can2, 2)

            # ***step3*** draw and present w.r.t the detection result
            map = gridmap(detc, grid)
            draw, _ = cellpresent(detc, grid, map, thresh)
            overlap = cv2.addWeighted(org1, 0.8, draw, 1, 0)

            # ***step4*** extract the connected areas
            overlap, etrs = extract(overlap, org2, draw, org2_path[0])

            # ***step5*** predict
            for i in range(len(etrs)):
                x = cv2.resize(etrs[i], (64, 64))
                x = np.reshape(x, (1, x.shape[0], x.shape[1], x.shape[2]))  # transform the x (64,64,3) into (1,64,64,3)
                drop_prob = 1  # the another input placeholder in this pretrained model
                pre = sess.run(y_pre, feed_dict={x_tf: x, drop_prob_tf: drop_prob})
                print(pre)
                if pre[0] == 1:
                    print("House")
                    cv2.putText(etrs[i], 'House', (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
                else:
                    print("Not Building")
                    cv2.putText(etrs[i], 'Not House', (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 255, 0), 2)

                cv2.imshow("extract" + str(i), etrs[i])
                cv2.moveWindow("extract" + str(i), 200 * i, 400)

            # ***step6*** show the result
            cv2.imshow("before", org1_copy)
            cv2.imshow("after", org2)
            cv2.imshow("paint", overlap)
            # cv2.imshow("result", detc)

            cv2.waitKey(800)

            print('running time: ', time.clock() - tmstart)



imgprocess()
