Engineering Log (Russell)
=

29/09/2018 -- 03/10/2018: Label and cut images for training
-
To train our neural network, we need to get the training data. That is:
1. 200 of block images
A block image is a unit which contains the areas which are house and not house. To a get better training effect, we need to clip the block image into several images
while each image only represents an area which is either a house or not a house.
2. A txt file to record the images for house and not house. 
For an individual image, use 1 to denote a house and 0 otherwise. 

e.g.

1_1 - The first area of block 

0   - not a house

1_2 - The second area of block 1

1   - a house

