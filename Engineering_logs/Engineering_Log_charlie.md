Engineering Log
=

10/08/2018 -- 11/08/2018: Research about Digital Cadastral Database (DCDB)
-
Since we are required to do the image/map difference detections in blocks, we find that the best way to manage a map into blocks format is by DCDB. It is a spatial location showing property boundaries normally in relation to adjoining properties, road easements and land. During the research, we found that the most efficient and accurate approach to achieve DCDB is Geojson file.

Research resource: 
+ Digital cadastral data:  
<https://www.business.qld.gov.au/running-business/support-assistance/mapping-data-imagery/data/digital-cadastral>
+ ACT Government Open Data Portal:  
<https://www.data.act.gov.au/>
+ ACTmapi:  
<http://www.actmapi.act.gov.au/download.html>

11/08/2018 -- 15/08/2018: Research about how to manage Geojson file
-
During the research, we have noticed that geojson file is an open standard format designed for representing simple geographical features, with non-spatial attributes. We can find that in the geojson file, we can extract coordinates of each polygons separately. Therefore, we can draw polygons on the image as well as the DCDB format. Besides, when we are able to identify each block by polygon, we are able to clip the image into blocks for later block detections.

Research resource: 
+ More than you ever wanted to know about GeoJSON:  
<https://macwright.org/2015/03/23/geojson-second-bite.html>
+ Tutorials - Managing Data Sources Using JSON files:  
<http://www.quadrigram.com/tutorials/managing-data-sources/using-json-files.html>
+ Converting GeoJSON to Python objects:  
<https://gis.stackexchange.com/questions/73768/converting-geojson-to-python-objects>

16/08/2018 -- 26/08/2018: Design Report Demo
-
To design our report demo, we need to demonstrate in few parts:
+ what can we do in general
+ what can we do in detail
+ what are our attractions

**In general**:  
We want to show the users a map with different colours of markers on map. The marker represents the block changes we have detected in amount of time. There are three colours, Red indicates unkown block changes; Green indicates building changes; Blue indicates tree changes. Besides, we will report statistics as well.

**In detail**:  
We want to show the specific block that we detected there is a change. We want to show the users where is the block (*Location*), when does it change (*Time*), what is the change (*Classification*).

**Attractions**:  
Why are we doing this?  
1. Large Area  
Human have ability to classify the changes, but not in large area. We are able to detect and identify changes efficiently in a relatively short amount of time. For example, we can manage the whole country, Australia, in 2 days. In contrast, it might takes 2 years for human.  
2. Localization  
We are able to identify specific block, and report its address to user. Therefore, users can keep track changes in blocks rather than anonymous random area.  
3. Auto  
The images can start detection and recognization at any time, any where without any operation and supervision.
4. Classification  
The highlight of our project is to classify changes. Since we detect any block changes, we are able to recognize the changes, such as tree, building and etc. It helps users to analysis constructions. For example, if a small area constructs several buildings in a short time, it trends to be a residental area or housing estate. It is highly likely to gain profit for building shopping mall, gym.

Resource:
+ Design on [Attractions](../Resources/Detection.png)
+ Design on [Report Demo](../Outcome/report_demo.pdf)

27/08/2018 -- 13/09/2018: Research about what is Deep Neural Network (DNN)
-
Our purpose is to identify objects in image, such as car, house, and so on. Neural networks are a set of algorithms, modeled loosely after the human brain, that are designed to recognize patterns. Neural networks help us cluster and classify. All classification tasks depend upon labeled datasets. Thus, we must transfer knowledge to the dataset in order for a neural to learn the correlation between labels and data. We collect and provide enough [dataset](../Code/Back-end/dataprocessing_python_QGIS/train_8047_8475.txt) for learning.

Deep Neural Network is composed of several layers. The layers are made of nodes. A node combines input from the data with a set of coefficients, or weights, that either amplify or dampen that input by computations, thereby assigning significance to inputs for the task the algorithm is trying to learn. These input-weight products are summed and the sum is passed through a node’s so-called activation function, to determine whether and to what extent that signal progresses further through the network to affect the ultimate outcome, which is classification.

To take image as input, we use matrices to represent such image. Color images can be represented by three matrices. Each matrix specifies the amount of red, green and blue that makes up the image. In turn, the elements of these matrices are integer numbers between 0 and 255, and they determine the intensity of the pixel with respect to the color of the matrix. A pixel is the smallest graphical element of a matricial image, which can take only one color at a time. For example, the image of 64 * 64 can be represented by a 64 * 64 * 3 matrix whose elements are the numbers 0 and 255.

Research resource: 
+ Matrices and Digital Images:  
<http://blog.kleinproject.org/?p=588>
+ A Beginner's Guide to Neural Networks and Deep Learning:  
<https://skymind.ai/wiki/neural-network>
+ Image Recognition:  
<https://www.tensorflow.org/tutorials/images/image_recognition>
+ DEEP LEARNING FOR IMAGE CLASSIFICATION:  
<https://www.nvidia.com/content/events/geoInt2015/LBrown_DL_Image_ClassificationGEOINT.pdf>

14/09/2018 -- 16/09/2018: Research about how to map Shapefile on Google Map
-
There are different approaches to achieve this. Since we use QGIS application to manage shapefile, there is a convenient way. QGIS supports multiple useful plugins. Qgis-openlayers-plugin is a QGIS plugin embedding OpenLayers functionality. It uses WebKit to render web based map services using their official Javascript API. Then we can display our shapefile on Google Map straightforward.

Here are the Steps: 
+ Install [qgis-openlayers-plugin](https://github.com/sourcepole/qgis-openlayers-plugin)
+ Add a new layer panel, Google Satellite
+ Overlay shapefile on Google Satellite

Research resource: 
+ Overlay Google Maps Using QGIS:   
<https://www.youtube.com/watch?v=h2ln3vAydOg>
+ qgis-openlayers-plugin:   
<https://github.com/sourcepole/qgis-openlayers-plugin>
+ How to import shapefile into Google Maps:  
<https://www.youtube.com/watch?v=FjRaX9ML3dE>
+ Display Shapefiles on Google Maps with this Google Mapplet:  
<http://dominoc925.blogspot.com/2012/12/display-shapefiles-on-google-maps-with.html>

17/09/2018 -- 19/09/2018: Extract amount of block id from Shapefile
-
Since there are huge amount of blocks in our shapefile, it will take long time to clip all the blocks. Moreover, we need to label enough blocks for later machine learning. It takes longer for us to label. As a prerequisite to have enough blocks for training, we decide to clip blocks in a specific area to save time. To achieve this, we need to extract all the block ids from this area.

Here are the Steps: 
+ select the area and create a new shapefile on this area
+ via QGIS API, extract all ids in this shapefile
+ alternative approach is via osgeo
+ output as a file  

[Source Code](../Code/Back-end/dataprocessing_python_QGIS/print_id.py)

Research resource: 
+ Writing Python Scripts for Processing Framework:   
<http://www.qgistutorials.com/en/docs/processing_python_scripts.html>
+ How to read a shapefile in Python:   
<https://gis.stackexchange.com/questions/113799/how-to-read-a-shapefile-in-python>
+ GDAL API Tutorial:  
<https://www.gdal.org/gdal_tutorial.html>

20/09/2018 -- 23/09/2018: Label images for training
-
We need to label all the clipped block images. For now, we only determine building, or not building. 1 indicates building, 0 otherwise. Since most of the blocks have at least one buildings, it affects our training accuracy. To make training more efficient and accurate, we decide to balance labels. To achieve this approach, we manually clip few images on block to specfiy area on images. For example, one block image. We clip this image into 3 smaller images. 2 images contains buildings, named as block_1, block_2, we label these as 1. The rest one maybe just some trees, named as block_3, we label it as 0. The format is:

e.g.  
1_1 - The first area of block  
0   - It is not a house  

1_2 - The second area of block 1  
1   - It is a house  

2_1 - The first area of block 2  
1   - It is a house

Therefore, we balance labels.  

*Input*: block images  
*Output*: a textfile with block id and its label line by line

It takes time to clip, name, label smaller images. I decide to use python code to simplify.

The idea for simplifying:
+ manually clip images
+ saves images in a folder 
+ label the image in textfile 
+ rename together as rename_x.jpg
+ automically rename the image to direct image name like 312_1.jpg
+ create a ID folder contains all the smaller images of that id 

[Source Code](../Code/Back-end/dataprocessing_python_QGIS/rename_2.py)

Research resource: 
+ How to copy and move files with Shutil:  
<https://www.pythonforbeginners.com/os/python-the-shutil-module>
+ Reading and Writing Files in Python:     
<https://www.pythonforbeginners.com/files/reading-and-writing-files-in-python>
+ How do I rename a file in python:  
<https://www.quora.com/How-do-I-rename-a-file-in-python>

24/09/2018 -- 05/10/2018: Design Poster
-
According to last semster poster, we decide to improve on it. To redesign our poster, we need to clearly demonstrate in few parts:
+ what is our project
+ what can we show
+ what techniques do we use
More detailed decisions and ideas applied in agenda.

Resource:
+ Design on [Poster](../Outcome/Semester%202%20Poster.pdf)

03/10/2018 -- 07/10/2018: Design Deliverable Report
-
Provide a deliverable report based on the design on report demo, but replace all the demo result and image to our actual result. We would like to show to the reader:
+ what are the area of differences between two images
+ what is the difference, the classification, and we decide to clip these differences to reader
+ a statistic on each classification

Resource:
+ Design on [Deliverable Report](../Outcome/Deliverable%20Report.pdf)