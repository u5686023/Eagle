Engineering Log
=

12/08/2018 -- 13/08/2018: Research about what tools are able to handle GEOTiff format images
-
During the research, we have found many useful applications that are able 
to handle GEOTiff files, such as QGIS, ARCGIS, GRASS GIS, OpenGeo Suit, 
Super GIS. After comparing the functionality, level of manipulation 
difficulty, number of tutorial materials and compatibility of different 
platforms, we have chosen QGIS as our main tool.

13/08/2018 -- 14/08/2018: Installation of QGIS and environment configuration
-
Install and Configure QGIS 3.2.2 from its official website (https://www.qgis.org/en/site/forusers/download.html).
Also, Install and Configure Python 3.7.0 and GDAL, since QGIS requires at least python 3.6 and GDAL module is an
important module which supports many important functionalities for QGIS.

14/08/2018 -- 17/08/2018: Study of QGIS tutorials
-
Go through basic tutorials of QGIS (https://www.qgistutorials.com/en/index.html):
1. Woring with attributes
2. Using plugins
3. Basic Vector styling
4. Basic raster styling and analysis
5. Raster mosaicing and clipping
6. Using python API
7. Building a Python Plugin

17/08/2018 -- 18/08/2018: Research about how to use python to clip raster layer with respect to shapefile
-
We found that we can use osgeo and gdal package to clip the raster layer. As long as we have installed gdal, we can
import that package in python. In that stage, we met several problems because of a mismatch between the version of 
GDAL and Python environment. Hence, we reinstalled reconfigured them to ensure that each team member in the technology 
department can successfully implement raster's clipping operation with python and gdal

18/08/2018 -- 20/08/2018: Finding sample data resource
-
For the test purpose, we need to have some sample data, that is, raster image and corresponding shapefile. Finally, we
have found them from the ACT Government's Open Geospatial Data Site (http://actmapi-actgov.opendata.arcgis.com).

18/08/2018 -- 21/08/2018: Develop the python code to clip raster layer
-
The basic idea is as followed:
1. A whole shapefile consists of many individual shapefile. For example, the shapefile of Canberra map consists of
shapefile of each block. Thus, we need to clip the whole shapefile and then make each individual shapefile as our 
output.
2. With these individual shapefiles, we thus are able to clip a specific section from the raster layer with respect to
the shapefile. In the example of Canberra map, we clip the image of each block (e.g. block id = 1,2,3,etc..) from the 
whole Canberra map.

21/08/2018 -- 24/08/2018: Convert to Tiff format
-
The code we have got in the previous step only allows us to clip Tiff format raster layer. However, the sample data we
obtained is of jp2 format. Hence, we need to figure out a way to convert from jp2 to Tiff. After going through the
documentation of gdal commands (https://www.gdal.org/gdal_utilities.html), we found we can use gdal_translate command
(https://www.gdal.org/gdal_translate.html) to do that.

27/08/2018 -- 15/09/2018: Research about Convolutional Neural Network (CNN)
-
In order to implement our classification function, we learned the basic concepts of CNN and tried to build a model for 
handwriting recognition.

Research resource:
1. TensorFlow Tutorial #02 Convolutional Neural Network:
<https://www.youtube.com/watch?v=HMcx-zY8JSg>
<https://github.com/Hvass-Labs/TensorFlow-Tutorials/blob/master/02_Convolutional_Neural_Network.ipynb>
2. Neural network realizes handwritten numeral recognition (MNIST):
<https://blog.csdn.net/xuanwolanxue/article/details/71565934>
3. THE MNIST DATABASE of handwritten digits:
<http://yann.lecun.com/exdb/mnist/>
4. Intro and preprocessing - Using Convolutional Neural Network to Identify Dogs vs Cats p. 1:
<https://www.youtube.com/watch?v=gT4F3HGYXf4>
5. Convolutional Neural Networks with TensorFlow - Deep Learning with Neural Networks 13:
<https://www.youtube.com/watch?v=mynJtLhhcXk>
6. Keras:
<https://blog.csdn.net/u013421629/article/details/79481452>
<https://machinelearningmastery.com/handwritten-digit-recognition-using-convolutional-neural-networks-python-keras/>
<https://blog.csdn.net/star_bob/article/details/48598417>
<https://keras.io/layers/convolutional/>
<https://www.jianshu.com/p/64172378a178>
<https://segmentfault.com/a/1190000012731665>
<https://blog.csdn.net/baimafujinji/article/details/78385745>

16/09/2018 -- 22/09/2018: Improve accuracy & Extract amount of block id from Shapefile
- 
We tested sample data and outputed correct results with acceptable accuracy. Then we improved accuracy to 90+%.
Also, in order to get input data for detection progress, we need to get the JPG for each block. 
Here's how we get it：
1. Find the scope of the tiff file we got earlier on shapefile.
2. Select the area and create a new shapefile on this area.
3. Via QGIS API, extract all ids in this shapefile.
4. Cut through id and convert to JPG

Research resource:
1. How to read a shapefile in Python:
<https://gis.stackexchange.com/questions/113799/how-to-read-a-shapefile-in-python>
2. GDAL API Tutorial:
<https://www.gdal.org/gdal_tutorial.html>

22/09/2018 -- 28/09/2018: Build usable dataset
-
Manually write the labels of the previously acquired block JPG to the textfile (1 indicates building, 0 otherwise).
Since most of the blocks have at least one buildings, it affects our training accuracy. To make training more 
efficient and accurate, we decide to balance labels. We manually clip few images on block to specfiy area on images. 
For example, one block image, We clip this image into 3 smaller images. 2 images contains buildings, named as 
block_1, block_2, we label these as 1. The rest named as block_3, we label it as 0. Hence, we could balance labels.
