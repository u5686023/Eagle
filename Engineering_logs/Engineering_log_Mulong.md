Engineering Log (Mulong)
=

13/09/2018 ： add the predict func into the CNn
-
Add the predict func into the cnn_precess_nn and cnn_process_model.

12/09/2018 : optimize the cnn
-
Add the dropout functionality into the model.

12/09/2018 : build the usable CNN model
-
3*conv / pool + 2*fc model.<br> 
x:(number_sample, img_fei, img_wid, channel).<br>
y:(number_sample, number_class).<br>
Bugs: 1. The y_hat and y_label should not be put into tf.transpose(); 
      2.The calculation of accuracy should use the tf.argmax(y,1)

11/09/2018 : Optimize the predict function
-
instead of fetching and reconstruct the graph using the get_tensor_byname() to fetch the operation directly

10/09/2018 : Add the prediction functionality and be able to save model 
-
Add the predict function. Add the saving trained model function to the ./trained_model folder

10/09/2018 : FIx bug of Classical DNN
-
Fix the incorrect accuracy bug after the first run 
problem: the initializer of the w and b was incorrect 
solution: replace the tf.get_variable with the tf.Variable + tf.random_normal as the initializer 

8/09/2018 : Build the Classical DNN
-
Build the Classical DNN and put on the same named folder.
Three .py: 1.process_image : Read and load the labeled images in the dataset
           2.process_NN : The three layer NN
           3.process_model : The executable file which calls the above files.