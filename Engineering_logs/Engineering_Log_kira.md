Engineering Log (Kira)
=

12/08/2018 -- 13/08/2018: Research about what tools are able to handle GEOTiff format images
-
During the research, we have found many useful applications that are able 
to handle GEOTiff files, such as QGIS, ARCGIS, GRASS GIS, OpenGeo Suit, 
Super GIS. After comparing the functionality, level of manipulation 
difficulty, number of tutorial materials and compatibility of different 
platforms, we have chosen QGIS as our main tool.

13/08/2018 -- 14/08/2018: Installation of QGIS and environment configuration
-
During the installation and configuration procedure. We have encountered
several problems. The main problems and solutions are as followed:
1. Python mismatch:
QGIS requires at least python 3.0. What's more, only the official version 
can be accepted (e.g. Anaconda would be rejected). Hence, as a solution, 
we just need to install the desired version of python and update the 
environment variables. 
2. GDAL modules not found:
GDAL module is an important module for QGIS. If we do not install GDAL, we do
can open QGIS. However, there would be several important functionalities that
we cannot use, such as raster clipping, merging and translating, which we
found later is important for our project.

14/08/2018 -- 17/08/2018: Study of QGIS tutorials
-
Go through basic tutorials of QGIS (https://www.qgistutorials.com/en/index.html):
1. Woring with attributes
2. Using plugins
3. Basic Vector styling
4. Basic raster styling and analysis
5. Raster mosaicing and clipping
6. Digitalizing map data
7. Using python API
8. Building a Python Plugin
9. Using custom Python expression functions

17/08/2018 -- 18/08/2018: Research about how to use python to clip raster layer with respect to shapefile
-
We found that we can use osgeo and gdal package to clip the raster layer. As long as we have installed gdal, we can
import that package in python.

18/08/2018 -- 20/08/2018: Finding sample data
-
For the test purpose, we need to have some sample data, that is, raster image and corresponding shapefile. Finally, we
have chosen the map of Canberra (http://actmapi-actgov.opendata.arcgis.com, 
http://actmapi-actgov.opendata.arcgis.com/datasets?q=administrative+boundaries&sort_by=relevance) and Brazil 
(https://earthdata.nasa.gov/earth-observation-data/near-real-time/rapid-response/modis-subsets,
https://lance-modis.eosdis.nasa.gov/imagery/subsets/?project=fas).

18/08/2018 -- 21/08/2018: Develop the python code to clip raster layer
-
The basic idea is as followed:
1. A whole shapefile consists of many individual shapefile. For example, the shapefile of Canberra map consists of
shapefile of each block. Thus, we need to clip the whole shapefile and then make each individual shapefile as our 
output.
2. With these individual shapefiles, we thus are able to clip a specific section from the raster layer with respect to
the shapefile. In the example of Canberra map, we clip the image of each block (e.g. block id = 1,2,3,etc..) from the 
whole Canberra map.

21/08/2018 -- 24/08/2018: Convert to Tiff format
-
The code we have got in the previous step only allows us to clip Tiff format raster layer. However, the sample data we
obtained is of jp2 format. Hence, we need to figure out a way to convert from jp2 to Tiff. After going through the
documentation of gdal commands (https://www.gdal.org/gdal_utilities.html), we found we can use gdal_translate command
(https://www.gdal.org/gdal_translate.html) to do that. 

02/09/2018 -- 03/09/2018: Convert Tiff to JPG format
-
The image we clipped from the original map is of Tiff format. However, the format of input for our image 
difference-detecting algorithm must be of JPG format. Basically, there are two ways to fix this problem.
The first one is to change the difference-detecting algorithm. However, this method is far more complicated
compared to converting the format of the image we clipped. Hence, we thus need to come up with a way to
convert tiff format to JPG format. After some researches, we found that we can use GDAL module to do that.

Research resources:
1. GDAL:
https://www.gdal.org/gdal_translate.html

27/08/2018 -- 10/09/2018: Research about Convolutional Neural Network (CNN)
-
In order to implement our classification function, we learned the basic concepts of CNN and tried to build a model for 
handwriting recognition. One of our group member has learned something about CNN, hence in our weekly Sunday meeting, he
runs a tutorial for us. During the tutorial, he introduced the basci knowledgement of CNN. After the 
meeting, we go through the following links to have a deeper understanding of CNN.

Research resource:
1. TensorFlow Tutorial #02 Convolutional Neural Network:
<https://www.youtube.com/watch?v=HMcx-zY8JSg>
<https://github.com/Hvass-Labs/TensorFlow-Tutorials/blob/master/02_Convolutional_Neural_Network.ipynb>
2. Neural network realizes handwritten numeral recognition (MNIST):
<https://blog.csdn.net/xuanwolanxue/article/details/71565934>
3. Intro and preprocessing - Using Convolutional Neural Network to Identify Dogs vs Cats p. 1:
<https://www.youtube.com/watch?v=gT4F3HGYXf4>
4. Convolutional Neural Networks with TensorFlow - Deep Learning with Neural Networks 13:
<https://www.youtube.com/watch?v=mynJtLhhcXk>
5. Keras:
<https://blog.csdn.net/u013421629/article/details/79481452>
<https://machinelearningmastery.com/handwritten-digit-recognition-using-convolutional-neural-networks-python-keras/>
<https://blog.csdn.net/star_bob/article/details/48598417>
<https://keras.io/layers/convolutional/>
<https://www.jianshu.com/p/64172378a178>
<https://segmentfault.com/a/1190000012731665>
<https://blog.csdn.net/baimafujinji/article/details/78385745>

11/09/2018 -- 20/09/2018: Develop a program to recognize handwritten digits
-
In order to have a deeper understanding of CNN model, we decide to do an exercise, that is, developing a
program to automatically recognize handwritten digits. We first collect some images of handwritten
digits as our program input. Thus, we develop the program.

Research resource:
1. THE MNIST DATABASE of handwritten digits:
<http://yann.lecun.com/exdb/mnist/>

20/09/2018 -- 23/09/2018: 
-
Our test map is part of the ACT while the shapefile includes all the area of ACT. Hence, 
we need to first get all the block IDs in the map and then put all the IDs in a txt file. My job is
the second step. That is, 
1. read all the IDs in the txt file 
2. select corresponding block shapefile from the whole shapefile
3. clip the image of each block with respect to its shapefile.

[Source Code](../Code/Back-end/dataprocessing_python_QGIS/test2.py)

26/09/2018 -- 03/10/2018: Label images for training
-
To train our neural network, we need to get the training data. That is:
1. A bunch of block images
A block image contains some area which is house and also some area which is not house. In
order to get better training effects, we need to clip the block image into several images
and each image only represents an area which is either a house or not a house.
2. A txt file recording whether each image is house or not
For an image, we use 1 to denote that it is a house and 0 otherwise. The format is:<br />

e.g.<br />
1_1 - The first area of block <br />
0   - It is not a house<br />

1_2 - The second area of block 1<br />
1   - It is a house<br />

2_1 - The first area of block 2<br />
1   - It is a house<br />

Each group member is asked to lable 200 images. However, after labling the first 30 images,
we found it too inefficient and time consuming. Hence, we decide to design a program to 
automatically do that for us.

[Source Code](../Code/Back-end/dataprocessing_python_QGIS/file_rename_move.py)

07/10/2018 -- 08/10/2018: Remove auto-produced images during the clipping process
-
When we clip the shape file and tiff file, there would be some auto produced files, like
"xxx.xml". Also, we only need jpg file for the neural network algorithm and therefore, the original
tiff file of each block is also useless. In order to save storage, we can remove all the others and
only keep the jpg file of each block during two different time period. However, if we do that manually,
it's extremely inefficient. Hence, we decide to write a program to automatically detect these redundant
files and remove them.

[Source Code](../Code/Back-end/dataprocessing_python_QGIS/remove_tiff.py)