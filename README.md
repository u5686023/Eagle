# ![civilise logo](Resources/Artboard_1ldpi.png)
### Look. Dream. Solve. 

Table of Contents
=================
  * [Hyperlinks](#hyperlinks)
  * [Introduction](#introduction)
  * [Customer Understanding](#stakeholders)
  * [Team](#team)
  * [System](#system)
  * [Progress and Planning](#progress-and-planning)
  * [Burn Down Chart](#burn-down-chart)
  * [User Story Map](#user-story-map)
  * [Risk Register](#risk-register)
  * [Decision Register](#decision-register)
  * [Task Priority](#task-priority)
  * [Audit 3 Report](#audit-3-report)
  * [Change Detection Report](#change-detection-report)
  * [Poster](#poster)
  
## Hyperlinks
*** Note all the documenation for audit 3 is located in Audit 3 directory on Eagle OneDrive. ***
#### [Eagle OneDrive](https://anu365-my.sharepoint.com/:f:/g/personal/u5686023_anu_edu_au/EpiN64dM_sJCqKXa4Fj-xGsBcQwZwvOCfgAdYTivYEfZjA?e=PIha52)


## Introduction
Eagle Unit, affiliated to Civilise.ai, is providing industrial level solutions on digital image processing and pattern recognition.
This is the audit landing page for Eagle's current project commenced February 24th, 2018.
On this page, the hyperlinks of Eagle's Gitlab Repository and OneDrive are provided to public visitors. 
Eagle updates the code development and maintance record in Gitlab Repository; posts administration, management and report documents on OneDrive. 
Internal communications within Eagle unit and Civilise.ai are powered by Slack and WeChat, 
whereas visitors are welcome to contact Guanzhou Charlie Chan (u5686023@anu.edu.au), the project manager of Eagle unit, 
for any question and concern about Eagle.
For any general inquiries about Civilise.ai group, please contact Mr.John Forbes (john.forbes@civilise.ai), 
the general manager of Civilise.ai, for further help and advise.

Click to watch the project algorithm video of Eagle

[![Watch the video](https://raw.github.com/GabLeRoux/WebMole/master/ressources/WebMole_Youtube_Video.png)](https://www.youtube.com/watch?v=2VpWmlaVy04)
[![Watch the video](https://raw.github.com/GabLeRoux/WebMole/master/ressources/WebMole_Youtube_Video.png)](https://youtu.be/LGVlaw-ShrI)

## Stakeholders

John Forbes operates Civilise.ai, outsources funding and potential customers for his company. Civilise.ai is dedicated to infrustructure planning and development.
Eagle Unit members extend from available resources, design and construct the software, providing consultancy services and
industrial level solutions to digital image processing and pattern recognition.
Users are the customers of Civilise.ai and the direct payers of the product. Users could come from different occupational field, hoping to
get improved image detecting efficiency and accuracy.
 
## Team
Our team meets regularly in person on Friday for tutorials and Saturdays for group work.

Team roles are defined as follows:

| Team Member            | Dev Role       |
| -----------------------| ---------------|
| Mulong Floze Xie       | Development Director<br>Scrum Master|
| Yuanxin Russell Ye     | Eagle Developer|
| Biwei Tara Cao         | Eagle Developer<br>Documentation|
| Yuan Claire Yao        | Eagle Developer|
| Shouxu Kira Lin        | Eagle Developer|
| Sidong Charlie Feng    | Eagle Developer|
| Guanzhou Charlie Chen  | Project Manager<br>Market Management|

## System

2018 S2
# ![System](Resources/SystemS2.jpg)

2018 S1
# ![System](Resources/System.png)


##  Progress and Planning

# ![System](Resources/Schedule5.png)


##  Burn Down Chart

# ![System](Resources/BDChart.png)


##  User Story Map

# ![System](Resources/usm3.png)

## Risk Register

 [Risk_Register.pdf](/Audit/Project_Risk_Register.pdf)

## Decision Register

 [Decision_Register.pdf](/Audit/Project_Decision_Register.pdf)

##  Task Priority
Task priority is defined as follows:

| Priority               | Task description       |
| -----------------------| ---------------|
| 1       | Basic technical requirement implementation: image clipping, Object classification, difference-detecting and "change summary report" generation|
| 2     | Business meeting with Queanbeyan|
| 3         | Innovation ACT|
| ----      | Audit 3 |
| 4        | Business meeting with Victorian councils|
| 5    | Website & Server development|


## Audit 3 Report
 [Audit3_report.pdf](/Audit/Project_Audit3.pdf)

## Change detection Report
 [Deliverable_Report.pdf](/Outcome/Deliverable_Report.pdf)

## Poster
 [Poster.pdf](/Outcome/Semester2_Poster.pdf)

## Detection
# ![System](Resources/Detection.png)

</br>
This landing page is created by [gh-md-toc](https://github.com/ekalinin/github-markdown-toc) with support from Team Oak





